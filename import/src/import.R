#
# Author: GJ
# Maintainers: GJ
# =======================================
# git/the_economist/import/src/import.R

rm(list=ls())
if(!require(pacman))install.packages("pacman")
pacman::p_load(tidyverse, janitor, here, data.table)

files <- list(graf1 = here("git/the_economist/import/input/Data for VJ trainees - Chart 1.csv"),
             graf2 = here("git/the_economist/import/input/Data for VJ trainees - Chart 2.csv"),
             graf3 = here("git/the_economist/import/input/Data for VJ trainees - Chart 3.csv"),
             graf4 = here("git/the_economist/import/input/Data for VJ trainees - Chart 4.csv"),
             out1 = here("git/the_economist/import/output/graf1.rds"),
             out2 = here("git/the_economist/import/output/graf2.rds"),
             out3 = here("git/the_economist/import/output/graf3.rds"),
             out4 = here("git/the_economist/import/output/graf4.rds"))

#Import data
data <- fread(files$graf1, skip=5)%>%
        clean_names()
saveRDS(data, files$out1)

data <- fread(files$graf2, skip=5)%>%
        clean_names()%>%
        mutate(year=gsub("forecast", "", year),
               year=as.numeric(year))
saveRDS(data, files$out2)

data <- fread(files$graf3, skip=5)%>%
        clean_names()
saveRDS(data, files$out3)

data <- fread(files$graf4, skip=5)%>%
        clean_names()%>%
        rename(country=location,
               one_vaccine_dose=share_of_population_with_at_least_one_vaccine_dose_percent,
               new_cases=daily_new_confirmed_case_per_1m_people)
saveRDS(data, files$out4)
